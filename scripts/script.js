let numberA;
let numberB;

let addition = "+";
let division = "/";
let subtraction = "-";
let multiplication = "*";

let result;

do {
    numberA = prompt("Enter a number A, please", [numberA]);
    numberB = prompt("Enter a number B, please", [numberB]);
} while (isNaN(numberA) || isNaN(numberB) || numberA == null || numberB == null || numberA.trim().length === 0 || numberB.trim().length === 0);

numberA = +numberA;
numberB = +numberB;

let action = prompt("What do you want to do with numbers? Choose one of theese '+, -, *, /'")

if (action == addition) {
    result = numberA + numberB;
    console.log(`The result is ${result}`);
} else if (action == division) {
    result = numberA / numberB;
    console.log(`The result is ${result}`);
} else if (action == subtraction) {
    result = numberA - numberB;
    console.log(`The result is ${result}`);
} else if (action == multiplication) {
    result = numberA * numberB;
    console.log(`The result is ${result}`)
} else {
    alert("You have done something wrong, please try again by reloading the page or pressing F5")
}